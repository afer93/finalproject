import React from "react";
import style from './Home.module.css';
import {Link} from 'react-router-dom'
import logoBasket from '../img/header/Vector.png'
import  MenuList from '../components/MenuList/MenuList'
import HomeCountCard from "../components/HomeCountCard/HomeCountCard";


function Home() {
    return (
        <div className={style.wrapper}>
            <div className={style.container}>
                <header className={style.header}>
                    <div className={style.title}>Наша продукция</div>
                    <div className={style.navigation}>
                        <HomeCountCard/>
                        <Link to="/basket">
                            <button className={style.btn_basket}>
                                <img src={logoBasket} alt={'basket'}/>
                            </button>
                        </Link>
                    </div>
                </header>
                <MenuList/>
                <MenuList/>
            </div>
        </div>
    );
}

export default Home;
