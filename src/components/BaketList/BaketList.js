import style from "./BaketList.module.css";
import BasketItem from "../BasketItem/BasketItem";
import dishes from "../dishes";
import React from "react";

const BasketList = () => {
    return (
        <ul className={style.product_basket__list}>
            { dishes.map((dish) => {
                return (
                    <BasketItem key = {dish.id}
                        id = {dish.id}
                        img = {dish.img}
                        alt = {dish.alt}
                        title = {dish.title}
                        description = {dish.description}
                        price = {dish.price}
                        scale = {dish.scale}/>
                )
            })}
        </ul>
    )
}
export default BasketList