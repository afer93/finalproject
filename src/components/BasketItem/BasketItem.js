import React from "react";
import style from "./BasketItem.module.css";

export default class BasketItem extends React.Component {
    render() {
        const {img, alt, title, price} = this.props;
        return (
            <li className={style.product_basket__list__item}>
                <img className={style.menu_item__img} src={img} alt={alt}/>
                <div className={style.menu_item__name}>{title}</div>
                <div className={style.menu_item__price}>{price}<span>₽</span></div>
                <button className={style.menu_item__del}>
                    -
                </button>
            </li>
        )
    }
}