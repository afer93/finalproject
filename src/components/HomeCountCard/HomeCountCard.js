import style from "../../pages/Home.module.css";
import React from "react";
import ButtonAdd from "../ButtonAdd/ButtonAdd";
import Counter from "../Counter";

export default class HomeCountCard extends React.Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         count: 0,
    //         price: 0
    //     }
    // }

    render() {
        // const {count, price} = this.props;
        return (
            <div className={style.choise}>
                <div className={style.products}><Counter/> товаров</div>
                <div className={style.price}>на сумму ₽</div>
            </div>
        )
    }
}