import React from "react";
import style from './MenuItem.module.css';
import ButtonAdd from "../ButtonAdd/ButtonAdd";


export default class MenuItem extends React.Component {
    render() {
        const { img, alt, title, description, price, scale } = this.props;
        return (
            <div className={style.item}>
                 <div className={style.item__img}>
                     <img src={img} alt={alt}/>
                 </div>
                 <div className={style.item__title}>{title}</div>
                 <div className={style.item__description}>{description}
                 </div>
                 <div className={style.item__price}>{price}
                     <div className={style.item__price_scale}>/ {scale}</div>
                 </div>
                 <div >
                     {/*<button className={style.btn_add}>+</button>*/}
                     <ButtonAdd className={style.btn_add}/>
                 </div>
             </div>
        )
    }
}